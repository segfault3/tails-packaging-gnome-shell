-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: gnome-shell
Binary: gnome-shell, gnome-shell-common
Architecture: linux-any all
Version: 3.22.3-3.0tails1
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Emilio Pozuelo Monfort <pochu@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/Projects/GnomeShell
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gnome-shell/
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gnome-shell
Build-Depends: debhelper (>= 10), gnome-control-center-dev (>= 1:3.4), gnome-pkg-tools (>= 0.11), gettext, pkg-config (>= 0.22), autoconf-archive, gobject-introspection (>= 1.49.1), gsettings-desktop-schemas-dev (>= 3.21.3), gtk-doc-tools, libatk-bridge2.0-dev, libcanberra-dev, libcanberra-gtk3-dev, libcroco3-dev (>= 0.6.8), libecal1.2-dev (>= 3.7.90), libedataserver1.2-dev (>= 3.17.2), libgcr-3-dev (>= 3.7.5), libgirepository1.0-dev (>= 1.29.15), libgjs-dev (>= 1.39.0), libglib2.0-dev (>= 2.45.3), libgnome-bluetooth-dev (>= 3.9.0) [linux-any], libgnome-desktop-3-dev (>= 3.7.90), libgnome-menu-3-dev, libgstreamer1.0-dev (>= 0.11.92), libgtk-3-dev (>= 3.21.6), libibus-1.0-dev, libmutter-dev (>= 3.22.1), libnm-glib-dev (>= 0.9.8) [linux-any], libnm-glib-vpn-dev (>= 0.9.8) [linux-any], libnm-gtk-dev (>= 0.9.8) [linux-any], libnm-util-dev (>= 0.9.8) [linux-any], libpolkit-agent-1-dev (>= 0.100), libpulse-dev (>= 2.0), libsecret-1-dev, libstartup-notification0-dev (>= 0.11), libsystemd-dev [linux-any], libtelepathy-glib-dev (>= 0.17.5), libx11-dev, libxfixes-dev, libxml2-dev, mesa-common-dev, python3
Package-List:
 gnome-shell deb gnome optional arch=linux-any
 gnome-shell-common deb gnome optional arch=all
Checksums-Sha1:
 0cfb5d638fb5b9ea02421fdab21c1ebfd145de06 1952576 gnome-shell_3.22.3.orig.tar.xz
 415c6c9025fa3cab7142604d7b637cab275fd030 163348 gnome-shell_3.22.3-3.0tails1.debian.tar.xz
Checksums-Sha256:
 d1e6bd80ddd1fef92d80b518d4dbeffa296e8f003402551b8c37c42744b7d42f 1952576 gnome-shell_3.22.3.orig.tar.xz
 4f0a75f7cab171020518af1d2caca7477bf3da52ed7eef48824fba461220ed37 163348 gnome-shell_3.22.3-3.0tails1.debian.tar.xz
Files:
 cb2d9ec7f80a72808d89f5db5560ece8 1952576 gnome-shell_3.22.3.orig.tar.xz
 82db743b2d16ec826c060cd8091c821f 163348 gnome-shell_3.22.3-3.0tails1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEcMAxqZeuB0p8dimNy2kI2AAOzf4FAlpc0h4ACgkQy2kI2AAO
zf43ihAAnkeS6Xnm9QvFVBjrtzUwX8apqiyaCSmIFSCAqSCACi3tFB2kY5PnwZhl
RJ5umXRezHxhWLAc/81DrSfGTYBpyulF/4HrtmFhGCI0QHTJwXNjWrMKKBPT1hjE
8+YkE4mxJfaOtqVWZWGAlR1dOq/2dze90ATKmkFgbDugjeY0CL6L5VZoLmFMRYph
nay0bnS093Joplw3H0tVJCzAeAeSDMueH9dJvh3nqDHx33OytLGDHgPB7dxLRq15
KJpsL/7sTMx7Sp7ByNEqkXonKt7VfDx7pn5Euir/8yoRGbm4fV54rP1SWWAI8+9S
StIA94G/hUvk8Cas/a8RyMY1i9nAdga3GgDmdph2Pef6xAvZ28eyphmek1YHJWMY
XsIMmmvVLNBLw9yUrSAiLzGtOTw3l9kcF5+8FXX6xivgQKI286u0n5fGjy1UAwJI
jF4/wG0qoCOfpnACSzO8w/N2nQSFySkshE1jnC/YGPwtRsA6icoSMImlvm95eHcS
Bu6Z32CgTIEIvlirOT42n75EuVzA+pwz/CEMzphrVjBx4YYzwmfF/oCuUubRem8s
XkDelQhxPtvLRRjJTi3d23KsZB7QLrgkYxThzCeR6EPkQnZHEbqjBxlf5xCVx17B
M6qnTT/Dvijdo98uVRP1jQYxCoLhs2NKEuFVYRDCt2A0pP7hj4E=
=sRwc
-----END PGP SIGNATURE-----
